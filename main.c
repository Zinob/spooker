/*
 * Code to play a using sound every WAKEUP_FREQUENCY time the light drops.
 *
 *           _______
 *           \     /
 *  __________|___|___.---||----+
 * |   ________________         |
 * |  |             VSS|--------'
 * |  |                |
 * |  |      light_grnd|--------+
 * '--|OUTPIN          |        A <<   photo diode
 *    |                |        |
 *    |      light_read|--------�
 *    |                |       |R| 400 kohm
 *    |                |       |_|
 *    |       light_pwr|------->'
 */

#include  <msp430g2553.h>
#include "sounds.h"

#define MCLK                    12000000
#define SAMPLES_PER_SECOND      8000

#define OUTPIN 			BIT2	//Which (TA1-PWM-enabled) pin to use for speaker
#define LIGHT_SENSOR_POWER BIT0	//Which pin should supply VCC for the light-sensor?
#define LIGHT_SENSOR_READ BIT4	//Which pin to use for reading the light-sensor?
#define LIGHT_SENSOR_GRN BIT5

#define DEBUG //+BIT6

#define STATE_STOP		0
#define STATE_START_PLAY		1
#define STATE_PLAYING	2
#define STATE_START_SAMPLING 3
#define STATE_SAMPLING	4

#define WAKEUP_FREQUENCY 7	//How often should we play sound when detecting LightOn?
#define SAMPLEAMOUNT 5		//How many samples to in order to determin if light is on?

unsigned int  currentSoundLenght;
const unsigned char*  currentSound;
volatile unsigned char RunState=0;
volatile unsigned char TimesWokenUp=0;
volatile unsigned char InLight=1;
unsigned char DarkCount=0;
unsigned char LightSamples=0;


//void playSound(const unsigned char sound[],unsigned int soundLenght);


void playSound(const unsigned char sound[],unsigned int soundLenght) {
	if (TA1CTL & MC_1) {
		return;
	} else {
		RunState=STATE_PLAYING;

		currentSoundLenght=soundLenght;
		currentSound=sound;
		//Set up the PWM-timer A0

		P1SEL |= OUTPIN;						// Setup OUTPIN for PWM output

		TA0CCR0= 255;
		TA0CCTL1 = OUTMOD_7;					// CCR1 reset/set
		TA0CCR1 = 0;							// CCR1 PWM duty cycle =0 at reset
		TA0CTL = TASSEL_2 + MC_1;				// SMCLK, upmode, and start it!
		//TA0CCTL0 = CCIE;						// CCR0 interrupt enabled

		//decoder-pace-timer set-up
		TA1CCTL0 = CCIE;						// CCR0 interrupt enabled
		TA1CCR0= MCLK / SAMPLES_PER_SECOND - 1;	// set CCR0 to the righ frequency Stolen from evilbetty
		TA1CTL = TASSEL_2 + MC_1;				// SMCLK, upmode, and start it!

		//Start all clocks and enable interrupts
		_BIS_SR(LPM0_bits + GIE);                 // Enter LPM0 w/ interrupt
	}
}

void SelectSound() {
	if (!(TimesWokenUp& WAKEUP_FREQUENCY)) {
		if (TimesWokenUp & (WAKEUP_FREQUENCY<<2) ){
			if (TimesWokenUp &1 )
			playSound(yawn, sizeof( yawn ) / sizeof( yawn[0] ));
		}else {
			playSound(laugh, sizeof( laugh ) / sizeof( laugh[0] ));
		}
	} else {
		RunState = STATE_STOP;
	}
	TimesWokenUp++;
}

void SampleLight() {
	if (LightSamples<=SAMPLEAMOUNT) {
		LightSamples++;
		if (ADC10MEM > 0x03F8){
			DarkCount++;
		}
		ADC10CTL0 |= ENC + ADC10SC;             //Start Next Sample.
		_BIS_SR(LPM3_bits + GIE);
	}else{
		/*DONE Sampling, deactivate light-sensor and increase WDT*/
		ADC10CTL0=0;						//Deactivate ADC
		P1OUT &= ~(LIGHT_SENSOR_POWER);  //Disable the power
		P1DIR &= ~(LIGHT_SENSOR_POWER +  LIGHT_SENSOR_GRN); //Since that does not seem to be enough, set high-impedance.
		WDTCTL=WDT_ADLY_1000;				//Increase WDT
		if ((DarkCount > SAMPLEAMOUNT - 1)) {
			if (InLight){
				RunState=STATE_START_PLAY;
			} else {
				RunState=STATE_STOP;
			}
			InLight=0;
		} else {
			InLight=1;
			RunState=STATE_STOP;
		}
	}
}

void StartSampling() {
	LightSamples=0;									//Ready for reading from light sensor
	DarkCount=0;
	P1DIR &= ~(LIGHT_SENSOR_READ);					//Ensure that read-pin is input
	P1DIR |= LIGHT_SENSOR_POWER +  LIGHT_SENSOR_GRN; // Set power and ground pins to output
	P1OUT |= LIGHT_SENSOR_POWER;					 //

	ADC10CTL0 = ADC10SHT_2 + ADC10ON + REFON + SREF_1 +ADC10SR ; // enable ADC
	ADC10CTL1 = INCH_4;                       // input have to match LIGHT_SENSOR_READ
	ADC10AE0 |= LIGHT_SENSOR_READ;

	WDTCTL = WDT_ADLY_1_9;							//Sample sort of often...
	_BIS_SR(LPM3_bits + GIE);				//Wait for the power to stabelize before the first stample
	ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start

	RunState = STATE_SAMPLING;
}

int unsigned playpointer=0;
void main(void) {
	//General setup
	WDTCTL = WDTPW + WDTHOLD;				// Stop WDT
	BCSCTL3 &= ~(LFXT1S_3);
	BCSCTL3 |= LFXT1S_2;

	BCSCTL1 = CALBC1_12MHZ + DIVA_2;					// Setup DCO //TO-DO increase DIVA done
	DCOCTL = CALDCO_12MHZ;

	P1DIR |= OUTPIN;
	P1OUT=0;


	//Set up Watchdog timer
	IE1 |= WDTIE;                             // Enable WDT interrupt
	WDTCTL = WDTSSEL + WDTTMSEL+WDTCNTCL +WDTPW ;	//Start the watchdog in interval-mode

	RunState=STATE_STOP;
	for (;;) {
		switch (RunState) {
		case STATE_START_PLAY:
			SelectSound();
			break;
		case STATE_PLAYING :
			_BIS_SR(LPM0_bits + GIE);
			break;
		case STATE_START_SAMPLING :
			StartSampling();
			_BIS_SR(LPM3_bits + GIE);
			break;
		case STATE_SAMPLING :
			SampleLight();
			break;
		case STATE_STOP:
			P1OUT = 0;
			TA0CTL &= ~(MC_3);			//Stop TA0
			TA1CTL &= ~(MC_3);			//Stop TA1
			TA0CCR1 = 0;
			P1SEL &= ~(OUTPIN);
			_BIS_SR(LPM3_bits + GIE);                 // Enter LPM3 w/ interrupt
			break;
		}
	}
}


#pragma vector=TIMER1_A0_VECTOR
__interrupt void Timer_1A (void) {
	if (playpointer>=currentSoundLenght || RunState!=STATE_PLAYING) {
		//If we are done playing stop all timers, set output to low and go in to LPM3
		playpointer=0;					//Reset the playpointer
		RunState=STATE_STOP;
		LPM0_EXIT;
	} else if (RunState==STATE_PLAYING){
		//If we have more bytes to play, change the PWM to the next value
		TA0CTL &= ~(MC_3)+TACLR;			//Stop TA0 before any modifications, as per SLAU144 p.378
		TA0CCR1=currentSound[playpointer++];	//Set the new TA0 duty-cycle
		TA0CTL = TASSEL_2 + MC_1;			//Start TA0 again
	}
}

// Timer A0 interrupt service routine
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_0A (void) {
}

#pragma vector=WDT_VECTOR
__interrupt void watchdog_timer(void) {
	if (RunState==STATE_STOP){
		RunState=STATE_START_SAMPLING;
	}
	LPM3_EXIT;
}
